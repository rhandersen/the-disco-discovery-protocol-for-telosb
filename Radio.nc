/*
 * Radio interface
 *
 */
#include "Beacon.h"

interface Radio {
	
	/*
	 * Turn on Beacon
	 */
	command void beaconOn();
	
	/*
	 * Turn off Beacon
	 */
	command void beaconOff();
	
	/*
	 * Set the beacon mode
	 * @true = beacon at both start and end
	 * @false = beacon at the start only
	 */
	command void setBeaconMode(bool mode);

	/*
	 * Message
	 */
	event void received(message_t* msg, void* buf, uint8_t len);
}