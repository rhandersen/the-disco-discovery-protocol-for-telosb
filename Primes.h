#ifndef PRIMES_H
#define PRIMES_H

typedef struct Primes {

	/*
	 * First prime
	 */
	uint16_t p1;
	
	/*
	 * Second prime
	 */
	uint16_t p2;
	
} Primes_t;

/*
 * Check if the remainder is zero
 */
bool remainderZero(Primes_t self, int counter)
{
	bool p1_val = ((counter % self.p1) == 0);
	bool p2_val = ((counter % self.p2) == 0);	
	
	return p1_val || p2_val;
}

#endif /* PRIMES_H */