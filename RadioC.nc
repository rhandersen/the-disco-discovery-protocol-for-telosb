/*
 * Radio Component
 */
#include "Beacon.h"
#include "printf.h"

module RadioC {
	provides interface Radio;
	uses {
		interface Packet;
		interface AMPacket;
		interface AMSend;
		interface SplitControl as AMControl;
		interface Receive;
		interface Leds;	
	}
}
implementation
{
	uint8_t package_id = 0;
	bool beaconMode = FALSE;
	bool busy = FALSE;
	bool powerDown = FALSE;
	message_t pkt;
	BeaconMsg* packet;

	void sendPackage(){
		if(!busy){
			busy = TRUE;
			packet = (BeaconMsg*)(call Packet.getPayload(&pkt, sizeof (BeaconMsg)));
			packet->source_node_id = TOS_NODE_ID;
			packet->package_id = package_id++;
			if (call AMSend.send(AM_BROADCAST_ADDR, &pkt, sizeof(BeaconMsg)) == SUCCESS) {
				#ifdef VERBOSE
				printf("Package sent from %d\n", TOS_NODE_ID);
  				printfflush();
  				#endif
			}	
		}		
	}

	command void Radio.beaconOn(){
		call AMControl.start();
	}
	
	command void Radio.beaconOff(){
		if(beaconMode == TRUE){
			sendPackage();
			powerDown = TRUE;
		}
		else {
			call AMControl.stop();
		}
	}

	command void Radio.setBeaconMode(bool mode){
		beaconMode = mode;
	}
	
	event void AMSend.sendDone(message_t *msg, error_t error){
		if(error == SUCCESS){
			call Leds.led0On();
			busy = FALSE;
		}
		if(powerDown){
			call AMControl.stop();
			powerDown = FALSE;
		}
	}

	event void AMControl.startDone(error_t error){
		if (error == SUCCESS) {
			if(beaconMode == TRUE){
				sendPackage();
			}
			else {
				#ifdef VERBOSE
				printf("Listening on node %d\n", TOS_NODE_ID);
  				printfflush();
  				#endif
			}
		}
		else {
			call AMControl.start();
		}
	}

	event void AMControl.stopDone(error_t error){
		call Leds.led0Off();
	}


	event message_t * Receive.receive(message_t *msg, void *payload, uint8_t len){
		signal Radio.received(msg,payload,len);
		return msg;
	}
}