/*
 * Discovery Implementaiton 
 */
#include "printf.h"
#include "Primes.h"

module DiscoveryC
{
  provides interface Discovery;
  
  uses {
  	interface Radio;
  	interface Primes;
  	interface Timer<TMilli> as Timer0;
  	interface Leds;
  }

}
implementation
{
	bool beaconMode = FALSE;
	bool beaconActive = FALSE;
	uint8_t m_dutyCycle = 0;
  	uint32_t m_counter = 0;
  	uint16_t m_timeslot = 0;
  	
  	// Primes
	Primes_t m_primes;
	
	/*
	 * Start discovery process
	 */
	command error_t Discovery.startDiscovery()
	{
		if(m_timeslot > 0 && m_primes.p1 > 0) {
			beaconActive = FALSE;
			m_counter = 0;
			
			call Timer0.startPeriodic(m_timeslot);
			return SUCCESS;
		}
		
		return FAIL;
	}

	/*
	 * TickHandler
	 */
	void tickHandler() 
	{
		++m_counter;
		
		if(!beaconActive)
		{
			if(remainderZero(m_primes,m_counter))
			{
				
#ifdef VERBOSE
				printf("%d mod %d / %d == 0 at node %d\n", m_counter, m_primes.p1, m_primes.p2, TOS_NODE_ID);
	  			printfflush();
#endif
				// Turn on the radio
				beaconActive = TRUE;
				call Radio.beaconOn();				
			}
				
		} 
		else 
		{
			// turn off radio
			beaconActive = FALSE;
			call Radio.beaconOff();
		}
	}
	
	/*
	 * Timer event fired
	 */
	event void Timer0.fired()
	{
		tickHandler();
	}

	/*
	 * Message received from radio
	 */
	event void Radio.received(message_t *msg, void *buf, uint8_t len){
		signal Discovery.received(msg,buf,len,m_counter,m_timeslot);
	}

	/*
	 * DutyCycle Getter
	 */
	command uint8_t Discovery.getDutyCycle(){
		return m_dutyCycle;
	}

	/*
	 * DutyCycle Setter
	 */
	command void Discovery.setDutyCycle(uint8_t dutycycle){
  		m_dutyCycle = dutycycle;
  		m_primes.p1 = call Primes.getPrime(dutycycle);
	}


	command void Discovery.setPrimes(uint16_t p1, uint16_t p2){
		m_primes.p1 = p1;
		m_primes.p2 = p2;
	}

	command error_t Discovery.setBeaconMode(bool beacon){
		beaconMode = beacon;
		call Radio.setBeaconMode(beacon);
		return SUCCESS;
	}

	command bool Discovery.getBeaconMode(){
		return beaconMode;
	}

	/*
	 * Timeslot Setter
	 * @param timeslot in milliseconds
	 */
	command void Discovery.setTimeSlot(uint16_t timeslot){
		m_timeslot = timeslot;
	}

	/*
	 * Timeslot Getter
	 */
	command uint16_t Discovery.getTimeSlot(){
		return m_timeslot;
	}

}

