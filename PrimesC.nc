/*
 * Prime Finder component
 */
module PrimesC {
	provides interface Primes;
}
implementation
{
	/**
	 * Checks if val is prime
	 */
	bool isPrime(uint16_t val)
	{
		int divs,square;

		if (val==2) return TRUE;    /* 2 is prime */
		if ((val&1)==0) return FALSE;    /* any other even number is not */

		divs=3;
		square=9;    /* 3*3 */
		while (square<val)
		{
			if (val % divs == 0) return FALSE;    /* evenly divisible */
			divs+=2;
			square=divs*divs;
		}
		if (square==val) return FALSE;
		
		return TRUE;

	}
	
	/*
	 * Get a primenumber
	 */
	command uint16_t Primes.getPrime(uint8_t dutyCycle)
	{
		uint16_t prime_prev=2, prime_cur=3;
		while(1)
		{
			if(isPrime(prime_cur)) {
				// Check if close to dutyCycle   
				if(abs((100/prime_prev) - dutyCycle) < abs((100/prime_cur) - dutyCycle)) {
					break;
				}
				prime_prev = prime_cur;
			}
			prime_cur += 2;
		}
		
		return prime_prev;
	}
}