/*
 * Primes interface
 */
interface Primes {
	
	/*
	 * getPrime:
	 *  Finds a prime that are close to the given dutycycle
	 */
	command uint16_t getPrime(uint8_t dutyCycle);
}