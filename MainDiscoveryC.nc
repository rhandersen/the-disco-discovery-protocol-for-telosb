/**
 *  The main module for discovery
 * 
 *  @author John 
 *  @author Henrik
 * 
 */
#include "printf.h"
#include "Beacon.h"

module MainDiscoveryC
{
  uses {
  	interface Leds;
  	interface Boot;
  	interface Discovery;
  } 
}
implementation
{
	bool beaconMode = TRUE;
  	int lastDiscovery=0;
  
  	/*
  	 * Device booted
  	 */
  	event void Boot.booted()
  	{
		uint8_t dutyCycle = 8;
 		uint16_t timeslot = 10;
  		uint16_t primes[2] = {47, 53};
  		
  		call Discovery.setDutyCycle(dutyCycle);
  		call Discovery.setTimeSlot(timeslot);
  		call Discovery.setBeaconMode(beaconMode);
  		call Discovery.setPrimes(primes[0], primes[1]);
  		call Discovery.startDiscovery();
  	
  		printf("# booted. beacon-mode: %d, dutycycle: %d, primes: %d, %d, timeslot: %d\n", beaconMode, dutyCycle, primes[0], primes[1], timeslot);
  		printfflush();

  	}
  
	/*
	 * Received a message from the discovery protocol
	 */
	event void Discovery.received(message_t *msg, void *buf, uint8_t len, int counter, int timeslot)
	{
		BeaconMsg *beacon = NULL;
		
		if (len == sizeof(BeaconMsg)) {
			beacon = (BeaconMsg*)buf;	
				
#ifdef VERBOSE
			printf("---------------- DISCOVERY ------------------\n");
			printf("Node %d discovered node %d\n", TOS_NODE_ID, msg->source_node_id);
			printf("Received package_id: %d, source: %d\n", msg->package_id, msg->source_node_id);
			printf("Timeslots since last discovery: %d\n", counter-lastDiscovery);
			printf("---------------- DISCOVERY ------------------\n");
			printfflush();
#endif	
		}
		
		printf("%ld,", (counter-lastDiscovery)*timeslot);
		printfflush();
		
		lastDiscovery = counter;		

	}
}

