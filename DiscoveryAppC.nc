configuration DiscoveryAppC {}
implementation
{
	/*
	 * Include the needed components
	 */
	
  	components MainC, MainDiscoveryC, LedsC;
  	components DiscoveryC,PrimesC,RadioC;
  	components new TimerMilliC() as Timer0;
  	components ActiveMessageC;
  	components new AMSenderC(6);
  	components new AMReceiverC(6);

  	/*
  	 * Wire up the interfaces
  	 */
  	MainDiscoveryC -> MainC.Boot;
  	MainDiscoveryC.Leds -> LedsC;
  	MainDiscoveryC.Discovery -> DiscoveryC;
  
  	DiscoveryC.Primes -> PrimesC;
  	DiscoveryC.Radio -> RadioC;
  	DiscoveryC.Primes -> PrimesC;
  	DiscoveryC.Timer0 -> Timer0;
  	DiscoveryC.Leds -> LedsC;
  
  	RadioC.Packet 	-> AMSenderC;
  	RadioC.AMPacket 	-> AMSenderC;
  	RadioC.AMSend		-> AMSenderC;
  	RadioC.AMControl 	-> ActiveMessageC;
  	RadioC.Receive 	-> AMReceiverC;
  	RadioC.Leds 		-> LedsC;
}

