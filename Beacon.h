#ifndef BEACON_H
#define BEACON_H

/*
 * BeaconMsg type for exchanging data 
 * between motes.
 */
typedef nx_struct BeaconMsg {
	nx_uint16_t package_id;
	nx_uint16_t source_node_id;
	nx_uint16_t destination_node_id;
} BeaconMsg;

#endif
