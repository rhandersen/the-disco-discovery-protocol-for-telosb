/*
 * Discovery protocol interface
 */
interface Discovery {
	
	/*
	 * Request a duty cycle between 0 and 100 percent
	 */
	command void setDutyCycle(uint8_t dutycycle);
	
	/*
	 * Get the current dutycycle
	 */
	command uint8_t getDutyCycle();
	
	/*
	 * Set primes
	 */
	command void setPrimes(uint16_t p1, uint16_t p2);

	/*
	 * Start discovery of neighbors
	 */
	command error_t startDiscovery();

	// Set the node class to reduce inter-class latency
	//command error_t setNodeClass(uint8_t classid);
	//command uint_t getNodeClass();
	
	/*
	 * Select beacon-and-listen or listen-only mode
	 */
	command error_t setBeaconMode(bool beacon);
	
	/*
	 * Get the current beaconmode
	 */
	command bool getBeaconMode();
	
	/*
	 * Set the timeslot
	 */
	command void setTimeSlot(uint16_t timeslot);
	
	/*
	 * Get the current timeslot
	 */
	command uint16_t getTimeSlot();
	
	/*
	 * Event for message received
	 */
	event void received(message_t *msg, void *buf, uint8_t len, int counter, int timeslot);
	
}